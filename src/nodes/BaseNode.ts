import { BaseInputType } from "./InputTypes.js";
import { NodeAbortReason } from "./NodeAbortReason.js";
import { NodeContext } from "./NodeContext.js";
import { BasePayloadType } from "./PayloadTypes.js";
import { BaseSlotArgsType, BaseSlotKeyType, BaseSlotType, ExtractSlotKeyTypeFromSlot, GetSlotKeysOfEmptySlotTypes } from './SlotTypes.js';

/**
 * Return type used for the loop of the node functions. This type can either be void, a full slot with its key and arguments or only the slot
 * key for those slots that do not have arguments.
 */
export type NodeLoopReturnType<SlotType> = void | GetSlotKeysOfEmptySlotTypes<SlotType> | SlotType;

/**
 * Helper type used for the function able to create a new Node class.
 */
type CreateBasicGraphNodeArguments<SlotKeyType extends BaseSlotKeyType, SlotArgsType extends BaseSlotArgsType, CreationArgumentsType, InitArgumentsType, RequiredPayloadType extends BasePayloadType, InternalDataType = undefined, MemoryInternalDataType extends {} = {}>  = {

    /**
     * The function that will be called each tick when this task is running. It receives three arguments: the first one contains the readonly
     * data of the node that is added during the construction of the node, the second one contains the context of the node (i.e., the slots that
     * are currently enabled, the payload and the graph call stack), and the third one is the object containing the memory being used by this 
     * node instance.
     * 
     * This function must return either void, the key of the Slot being fired in this iteration, or an object containing this key and arguments
     * for the transition being run.
     */
    loopFunction: (nodeData: Readonly<InternalDataType>, context: NodeContext<SlotKeyType, RequiredPayloadType>, memory: MemoryInternalDataType) => NodeLoopReturnType<BaseSlotType<SlotKeyType, SlotArgsType>>,
    
    /**
     * The function that will be called whenever the node gets instantiated. The same arguments that this function receives will need 
     * to be supplied by the constructor when this node is instantiated. The function returns the constant internal data state that will
     * be provided to both the loop and start functions.
     */
    nodeCreationFunction?: (args: Readonly<CreationArgumentsType>) => InternalDataType

    /**
     * The function that will be called when the node starts its execution (i.e. a transition that leads to this node is fired). As arguments,
     * it receives both the arguments from the transition and the constant internal node data. The function must return the initial value for the
     * memory of the node.
     */
    nodeStartFunction?: (nodeData: Readonly<InternalDataType>, transitionArguments?: Readonly<InitArgumentsType>) => MemoryInternalDataType;

    /**
     * The function that will be called if the execution of this node gets aborted. This function can be used to free any resources allocated by the
     * node if its execution gets interrupted.
     */
    nodeAbortFunction?: (reason: NodeAbortReason, memory: MemoryInternalDataType) => void;

};

/**
 * Base class used to implement all kind of nodes. These can be either wrappers, graphs or leaf tasks. It receives 
 * @typeParam SlotType The type containing an union of all the slots that can have a transition attached into this node.
 * @typeParam InputType The dynamic input type specifying the shape that the data coming from a transition can have.
 * @typeParam RequiredPayloadType The type containing an intersection of all the payloads required for the execution of this node.
 * @typeParam MemoryInternalDataType The type of the memory object that will store the agent information for this task between iterations.
 */
export abstract class BaseNode<SlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>, InputType extends BaseInputType, RequiredPayloadType extends BasePayloadType, MemoryInternalDataType extends {} = {}> {
        
    // Quick-hack. Since TypeScript implements Duck-typing, it is necessary to store somewhere both the SlotType and RequirePayloadType in order
    // to be able to infer these type parameters. In runtime, thess fields do not exist in the objects.
    _slotTypes?: SlotType;
    _payload: RequiredPayloadType;

    abstract start(args: InputType): MemoryInternalDataType;
    
    abstract loop(context: NodeContext<ExtractSlotKeyTypeFromSlot<SlotType>, RequiredPayloadType>, memory: MemoryInternalDataType): NodeLoopReturnType<SlotType>;

    abstract abort(reason: NodeAbortReason, memory: MemoryInternalDataType): void;


    static createBasicGraphNode
        <SlotKeyType extends BaseSlotKeyType, SlotArgsType extends BaseSlotArgsType, CreationArgumentsType, InitArgumentsType, RequiredPayloadType extends BasePayloadType, InternalDataType = undefined, MemoryInternalDataType extends {} = {}>
        
        (args: CreateBasicGraphNodeArguments<SlotKeyType, SlotArgsType, CreationArgumentsType, InitArgumentsType, RequiredPayloadType, InternalDataType, MemoryInternalDataType>): new (args: CreationArgumentsType) => 
            BaseNode<BaseSlotType<SlotKeyType, SlotArgsType>, InitArgumentsType, RequiredPayloadType> {

        
        class BasicGraphNode extends BaseNode<BaseSlotType<SlotKeyType, SlotArgsType>, InitArgumentsType, RequiredPayloadType, MemoryInternalDataType> {
            
            loop(context: NodeContext<ExtractSlotKeyTypeFromSlot<BaseSlotType<SlotKeyType, SlotArgsType>>, any>, memory: MemoryInternalDataType): NodeLoopReturnType<BaseSlotType<SlotKeyType, SlotArgsType>> {
                const result = args.loopFunction(this.internalData, context, memory);
                
                if (typeof result === "string"){
                    if (context.slotIsEnabled(result as unknown as ExtractSlotKeyTypeFromSlot<BaseSlotType<SlotKeyType, SlotArgsType>>)){
                        return result;
                    } else {
                        console.log("[WARN] Trying to return from unregistered slot. Ignoring slot: " + result);
                    }
                } else {
                    const aux = result as {key: BaseSlotKeyType, args: BaseSlotArgsType};
                    if ((aux.key !== undefined)){
                        if (context.slotIsEnabled(aux.key as ExtractSlotKeyTypeFromSlot<BaseSlotType<SlotKeyType, SlotArgsType>>)){
                            return aux as NodeLoopReturnType<BaseSlotType<SlotKeyType, SlotArgsType>>;
                        } else {
                            console.log("[WARN] Trying to return from unregistered slot. Ignoring slot: " + aux.key);
                        }
                    }
                }
            }
            
            abort(reason: NodeAbortReason, memory: MemoryInternalDataType): void {
                if (args.nodeAbortFunction !== undefined){
                    return args.nodeAbortFunction(reason, memory);
                }
            }

            private internalData: InternalDataType

            start(_args: InitArgumentsType): MemoryInternalDataType {
                if (args.nodeStartFunction !== undefined){
                    return args.nodeStartFunction(this.internalData, _args);
                }

                return {} as MemoryInternalDataType;
            }

            constructor(initArgs: CreationArgumentsType){
                super();
                if (args.nodeCreationFunction !== undefined){
                    this.internalData = args.nodeCreationFunction(initArgs);
                } else {
                    this.internalData = {} as InternalDataType;
                }
            }

        }

        return BasicGraphNode;
    }
}

