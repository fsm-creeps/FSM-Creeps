import { AnyNode } from "../../nodes/AnyNode.js";
import { BaseNode } from "../../nodes/BaseNode.js";
import { BaseInputType, ExtractNodeInputType } from "../../nodes/InputTypes.js";
import { ExtractMemoryType } from "../../nodes/MemoryTypes.js";
import { NodeAbortReason } from "../../nodes/NodeAbortReason.js";
import { NodeContext } from "../../nodes/NodeContext.js";
import { BasePayloadType, ExtractNodePayloadType } from "../../nodes/PayloadTypes.js";
import { BaseSlotArgsType, BaseSlotKeyType, BaseSlotType, ExtractSlotKeyTypeFromSlot, ExtractSlotType, GetSlotKeysOfEmptySlotTypes } from "../../nodes/SlotTypes.js";
import { WrapperMemoryContainer } from "./WrapperMemory.js";
import { NodeLoopReturnType } from '../BaseNode.js';
import { EmptySlotType } from '../SlotTypes.js';

    
type CreateWrapperArguments<WrapperSlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>, WrapperCreationArgumentsType, WrapperInitArgumentsType, WrapperPayloadType,
    WrapperInternalDataType = undefined, WrapperMemoryInternalDataType extends {} = {}> = {
        
    /**
     * The function that will be called each tick when this wrapper is running. It receives three arguments: the first one contains the readonly
     * data of the wrapper that is added during the instance creation, the second one contains the context of the node (i.e., the slots that are currently 
     * enabled), and the third one is the object containing the memory being used by this node instance.
     * 
     * This function must return either the new value for the memory object (that will be used in further iterations), the key of the Slot 
     * being fired in this iteration, or an object containing this key and arguments for the transition being run.
     */
    loopFunction: (wrapperData: Readonly<WrapperInternalDataType>, context: NodeContext<ExtractSlotKeyTypeFromSlot<WrapperSlotType>, WrapperPayloadType>, memory: WrapperMemoryInternalDataType) =>
        WrapperSlotType | GetSlotKeysOfEmptySlotTypes<WrapperSlotType> | void;

    /**
     * The function that will be called whenever the wrapper gets instantiated. The same arguments that this function receives will need 
     * to be supplied by the constructor when this node is instantiated. The function returns the constant internal data state that will
     * be provided to both the loop and start functions.
     */
    wrapperCreationFunction?: (args: Readonly<WrapperCreationArgumentsType>) => WrapperInternalDataType;

    /**
     * The function that will be called if the execution of this wrapper gets aborted. This function can be used to free any resources allocated by the
     * wrapper if its execution gets interrupted.
     */
    abortFunction?: (reason: NodeAbortReason, memory: WrapperMemoryInternalDataType) => void;

    /**
     * The function that will be called when the wrapper starts its execution (i.e. a transition that leads to this wrapper is fired). As arguments,
     * it receives both the arguments from the transition and the constant internal wrapper data. The function must return the initial value for the
     * memory of the node.
     */
    wrapperStartFunction?: (wrapperData: Readonly<WrapperInternalDataType>, transitionArguments?: Readonly<WrapperInitArgumentsType>) => WrapperMemoryInternalDataType;

};

/**
 * Helper type to combine the slot types of the wrapper and the node being wrapped. It will make an union of both types, unless one of the types is not
 * explicitly defined in which case the other slot type will be used.
 */
type CombineWrappedAndWrappedSlotTypes<WrapperSlotType extends BaseSlotType<BaseSlotKeyType>, WrappedSlotType extends BaseSlotType<BaseSlotKeyType>> = 
    EmptySlotType<BaseSlotKeyType> extends WrappedSlotType ? WrapperSlotType :  (EmptySlotType<BaseSlotKeyType> extends WrapperSlotType ? WrappedSlotType : (WrappedSlotType | WrapperSlotType));


/**
 * Base class for all the wrappers used in this library.
 * @typeParam WrappedSlotType Slot type for the wrapped node.
 * @typeParam WrappedInputType Input type for the wrapped node.
 * @typeParam WrappedPayloadType Payload type for the wrapped node.
 * @typeParam WrappedMemory Memory type for the wrapped node.
 * @typeParam WrapperSlotType Slot type for the wrapper.
 * @typeParam WrapperInputType Input type for the wrapper.
 * @typeParam WrapperPayloadType Payload type for the wrapper.
 * @typeParam WrapperMemory Memory type for the wrapper.
 */
export abstract class NodeWrapper<
    WrappedSlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>,
    WrappedInputType extends BaseInputType,
    WrappedPayloadType extends BasePayloadType,
    WrappedMemory extends {},
    WrapperSlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>,
    WrapperInputType extends BaseInputType, 
    WrapperPayloadType extends BasePayloadType,
    WrapperMemory extends {}>
    
    
    extends BaseNode<CombineWrappedAndWrappedSlotTypes<WrapperSlotType, WrappedSlotType>, WrapperInputType, WrapperPayloadType & WrappedPayloadType, WrapperMemoryContainer<WrappedMemory, WrapperMemory>> {
    
    // Auxiliary types to help in the inference of the types for validation purposes of the graphs.
    _slotTypes?: CombineWrappedAndWrappedSlotTypes<WrapperSlotType, WrappedSlotType>;
    _wrapperSlots: WrapperSlotType;
    _wrappedSlots: WrappedSlotType;
    _payload: WrapperPayloadType & WrappedPayloadType;
    _wrapperPayload: WrapperPayloadType;
    _wrappedPayload: WrappedPayloadType;

    override start(args: WrapperInputType): WrapperMemoryContainer<WrappedMemory, WrapperMemory> {
        
        return {
            wrappedMemory: this.wrappedNode.start(this.preprocessDynamicInput(args)),
            wrapperMemory: this.wrapperStart(args)
        }


    }

    override loop(context: NodeContext<ExtractSlotKeyTypeFromSlot<WrappedSlotType> | ExtractSlotKeyTypeFromSlot<WrapperSlotType>, WrapperPayloadType>, memory: WrapperMemoryContainer<WrappedMemory, WrapperMemory>): 
        NodeLoopReturnType<CombineWrappedAndWrappedSlotTypes<WrapperSlotType, WrappedSlotType>> {
        
        const wrapperResult = this.wrapperLoop(context as NodeContext<ExtractSlotKeyTypeFromSlot<WrapperSlotType>, any>, memory.wrapperMemory);

        if (wrapperResult !== undefined){
            
            // Transition fired from wrapper
            this.wrappedNode.abort("AbortedByWrapper", memory.wrappedMemory);
            return wrapperResult as NodeLoopReturnType<CombineWrappedAndWrappedSlotTypes<WrapperSlotType, WrappedSlotType>>;

        } else {
            const wrappedResult = this.wrappedNode.loop(context as NodeContext<ExtractSlotKeyTypeFromSlot<WrappedSlotType>, any>, memory.wrappedMemory);
            
            if (wrappedResult !== undefined){

                // Transition fired from wrapped
                this.wrapperAbort("AbortedByWrapped", memory.wrapperMemory);
                return wrappedResult as NodeLoopReturnType<CombineWrappedAndWrappedSlotTypes<WrapperSlotType, WrappedSlotType>>;

            }

        }
    }

    override abort(reason: NodeAbortReason, memory: WrapperMemoryContainer<WrappedMemory, WrapperMemory>): void {
        this.wrapperAbort(reason, memory.wrapperMemory);
        this.wrappedNode.abort(reason, memory.wrappedMemory);
    }


    /**
     * The start method for the wrapper.
     * @param args The dynamic input arguments.
     * @returns The initial memory value.
     */
    abstract wrapperStart(args: WrapperInputType): WrapperMemory;

    /**
     * The loop method for the wrapper.
     * @param context The context of the execution.
     * @param memory The memory value for the wrapper.
     * @returns void or the transition to be fired.
     */
    abstract wrapperLoop(context: NodeContext<ExtractSlotKeyTypeFromSlot<WrapperSlotType>, any>,  memory: WrapperMemory): WrapperSlotType | GetSlotKeysOfEmptySlotTypes<WrapperSlotType> | void;

    /**
     * The abort method for the wrapper.
     * @param reason The reason of the abortion.
     * @param memory The memory of the wrapper.
     */
    abstract wrapperAbort(reason: NodeAbortReason, memory: WrapperMemory);

    /**
     * This function should preprocess the dynamic input of the wrapper before dispatching it into the wrapped node.
     * @param args The dynamic input of the wrapper.
     * @returns The dynamic input for the wrapped node.
     */
    abstract preprocessDynamicInput(args: WrapperInputType): WrappedInputType;

    /**
     * Wrapped node of node wrapper.
     */
    private readonly wrappedNode: BaseNode<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory>;
    

    _wrappedNodeTypeContainer: BaseNode<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory>; // used only for type inference


    /**
     * Constructor for the wrapper class.
     * @param wrappedNode The node being wrapped by this wrapper.
     */
    protected constructor(wrappedNode: BaseNode<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory>){

        super();

        this.wrappedNode = wrappedNode;
    }

    
    public static createBasicWrapper<WrapperSlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>, WrapperInitArgumentsType, WrapperPayloadType extends BasePayloadType = BasePayloadType, WrapperCreationArgumentsType = void,
        WrapperInternalDataType = undefined, WrapperMemoryInternalDataType extends {} = {}>

        (creationArguments: CreateWrapperArguments<WrapperSlotType, WrapperCreationArgumentsType, WrapperInitArgumentsType, WrapperPayloadType, WrapperInternalDataType, WrapperMemoryInternalDataType>): 
            new <WrappedSlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>, WrappedInputType extends BaseInputType, WrappedPayloadType extends BasePayloadType, WrappedMemory extends {}>
                (node: BaseNode<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory>, args: WrapperCreationArgumentsType) => NodeWrapper<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory, WrapperSlotType, WrapperInitArgumentsType, WrapperPayloadType, WrapperMemoryInternalDataType> {

                    
                class Aux<
                    WrappedNode extends AnyNode,
                    WrappedSlotType extends BaseSlotType<BaseSlotKeyType, BaseSlotArgsType> = ExtractSlotType<WrappedNode>, 
                    WrappedInputType extends BaseInputType = ExtractNodeInputType<WrappedNode>, 
                    WrappedPayloadType extends BasePayloadType = ExtractNodePayloadType<WrappedNode>, 
                    WrappedMemory extends {} = ExtractMemoryType<WrappedNode>>
                    
                    extends NodeWrapper<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory, WrapperSlotType, WrapperInitArgumentsType, WrapperPayloadType, WrapperMemoryInternalDataType> {

                    private readonly preprocessCallback?: (args: WrapperInitArgumentsType) => ExtractNodeInputType<WrappedNode>;
                    private readonly wrapperData: WrapperInternalDataType;
                    
                    
                    preprocessDynamicInput(args: WrapperInitArgumentsType): ExtractNodeInputType<WrappedNode> {
                        if (this.preprocessCallback !== undefined){
                            return this.preprocessCallback(args);
                        } else {
                            return args as unknown as  ExtractNodeInputType<WrappedNode>;
                        }
                    }
                    
                    wrapperStart(args: WrapperInitArgumentsType): WrapperMemoryInternalDataType {
                        if (creationArguments.wrapperStartFunction !== undefined){
                            return creationArguments.wrapperStartFunction(this.wrapperData, args);
                        } else {
                            return {} as WrapperMemoryInternalDataType;
                        }
                    }

                    wrapperLoop(context: NodeContext<ExtractSlotKeyTypeFromSlot<WrapperSlotType>, any>, memory: WrapperMemoryInternalDataType): WrapperSlotType | void | GetSlotKeysOfEmptySlotTypes<WrapperSlotType> {
                        
                        
                        return creationArguments.loopFunction(this.wrapperData, context, memory);
                    }

                    wrapperAbort(reason: NodeAbortReason, memory: WrapperMemoryInternalDataType) {
                        if (creationArguments.abortFunction !== undefined){
                            return creationArguments.abortFunction(reason, memory);
                        }
                    }

                    constructor(node: BaseNode<WrappedSlotType, WrappedInputType, WrappedPayloadType, WrappedMemory>, args: WrapperCreationArgumentsType){
                        super(node);

                        if (creationArguments.wrapperCreationFunction !== undefined){
                            this.wrapperData = creationArguments.wrapperCreationFunction(args);
                        }
                    }
                }


                return Aux;
        
    }
}

/**
 * Helper type that matches all the wrappers defined through this library.
 */
export type AnyWrapper = NodeWrapper<BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>, BaseInputType, BasePayloadType, {}, BaseSlotType<BaseSlotKeyType, BaseSlotArgsType>, BaseInputType, BasePayloadType, {}>;

/**
 * Helper type to infer the type of the BaseNode being extended by a given wrapper type.
 */
export type CastWrapperToBaseNode<Wrapper extends AnyWrapper> =
    Wrapper extends BaseNode<infer SlotType, infer InputType, infer PayloadType, infer MemoryType> ? BaseNode<SlotType, InputType, PayloadType, MemoryType> : never;

/**
 * Helper type to infer the slot type of a given wrapper type.
 */
export type GetSlotType<Wrapper> = Wrapper extends BaseNode<infer SlotType, BaseInputType, BasePayloadType, {}> ? SlotType : never;